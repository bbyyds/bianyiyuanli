#include "AST.h"

AST::AST(){
    this->content = "Empty";
    this->parent = NULL;
    this->follow = NULL;
    this->first = NULL;
    this->depth = 0;
}
AST::AST(nodeType type, const char* content){
    this->type = type;
    this->content = content;
    this->parent = NULL;
    this->follow = NULL;
    this->first = NULL;
    this->depth = 0;
}
int AST::getType(){
    return this->type;
}
AST* AST::getParent(){
    return this->parent;
}
AST* AST::getFollow(){
    return this->follow;
}
AST* AST::getFirst(){
    return this->first;
}
int AST::getDepth(){
    return this->depth;
}
string AST::getContent(){
    return this->content;
}
// 与.h文件中的枚举类对应
string AST::getTypeName(){
    switch(this->type){
        case 0: return "ROOT";
        case 1: return "ID";
        case 2: return "DEF";
        case 3: return "EXPR";
        case 4: return "STMT";
        case 5: return "OP";
        case 6: return "COMPST";
        case 7: return "MODIFY";
        case 8: return "CONST_INT";
        case 9: return "ARRAY";
        case 10: return "CALL";
        case 11: return "CONST_STR";
        case 12: return "FORMAT_INT";
        default: return "";
    }
}
// 插入首个子结点
void AST::appendFirst(AST* first){
    if(first){
        this->first = first;
        first->parent = this;
        AST* node = first->follow;
        while(node){
            node->parent = this;
            node = node->follow;
        }
        updateDepth(first);
    }
    else{
        printf("error first\n");
    }
}
// 插入一个兄弟节点
void AST::appendFollow(AST* follow){
    if(follow){
        this->follow = follow;
        follow->parent = this->parent;
        updateDepth(follow);
    }
    else{
        printf("error follow\n");
    }
}
void AST::setDepth(int depth){
    this->depth = depth;
}
void updateDepth(AST* node){
    if(node!=NULL){
        node->setDepth(node->getParent()->getDepth()+1);
        updateDepth(node->getFollow());
        updateDepth(node->getFirst());
    }
    return;
}
// 打印结点的信息
void AST::printNode(){
    cout << "Type:" << this->getTypeName();
    cout << ", Content:" << this->content;
    cout << ", Depth:" << this->depth;
}
// 先序遍历，分层次打印语法树
void printTree(AST* node){
    if(node!=NULL){
        for(int i=0;i<node->getDepth();i++){
            cout << " |";
        }
        cout << "-*";
        node->printNode();
        cout << endl;
        printTree(node->getFirst());
        printTree(node->getFollow());
    }
    return;
}
