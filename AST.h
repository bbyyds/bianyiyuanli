#include <iostream>
using namespace std;

// 枚举类，用于规定语法树中结点的类型
enum nodeType{
    ROOT, ID, DEF, EXPR, STMT, OP, COMPST, MODIFY, CONST_INT, ARRAY, CALL, CONST_STR, FORMAT_INT
};

class AST{
private:
    nodeType type;
    AST* parent;
    AST* follow;
    AST* first;  
    int depth;
    string content;
public:
    AST();
    AST(nodeType type, const char* str);
    int getType();
    AST* getParent();
    AST* getFollow();
    AST* getFirst();
    int getDepth();
    string getContent();
    string getTypeName();
    void appendFirst(AST* first);
    void appendFollow(AST* follow);
    void printNode();
    void setDepth(int depth);
};
void updateDepth(AST* node);
void printTree(AST* root);
