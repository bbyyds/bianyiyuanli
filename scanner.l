%option noyywrap
LETTER [a-zA-Z_]
DIGIT [0-9]
WHITE_SPACE [ \t\n] 
%{
#include <stdio.h>
#include "parser.tab.h"
#include <string.h>
using namespace std;
%}

%%
"int"       { /*printf("Int\n");*/ return INT; }
"="         { /*printf("Assign\n");*/ return ASSIGN; }
"for"       { /*printf("For\n");*/ return FOR; }
"while"     { /*printf("While\n");*/ return WHILE; }
"if"        { /*printf("If\n");*/ return IF; }
"else"      { /*printf("Else\n");*/ return ELSE; }
"scanf"     { /*printf("Scanf\n");*/ return SCANF; }
"printf"    { /*printf("Printf\n");*/ return PRINTF; }
"return"    { /*printf("Return\n");*/ return RETURN; }

"{"         { /*printf("{\n");*/ return '{'; }
"}"         { /*printf("}\n");*/ return '}'; }
"("         { /*printf("(\n");*/ return '('; }
")"         { /*printf(")\n");*/ return ')'; }
","         { /*printf("Comma\n");*/ return COMMA; }
";"         { /*printf("Semi\n");*/ return SEMICOLON; }
"\""        { /*printf("D_quo\n");*/ return D_QUOTES; }

"+"         { /*printf("Add\n");*/ return ADD; }
"-"         { /*printf("Sub\n");*/ return SUB; }
"*"         { /*printf("Mul\n");*/ return MUL; }
"/"         { /*printf("Div\n");*/ return DIV; }
"%"         { /*printf("Mod\n");*/ return MOD; }
"++"        { /*printf("Double_Add\n");*/ return DOUBLE_ADD; }

"=="        { /*printf("EQ\n");*/ return EQ; }
">"         { /*printf("GT\n");*/ return GT; }
"<"         { /*printf("LT\n");*/ return LT; }
">="        { /*printf("GE\n");*/ return GE; }
"<="        { /*printf("LE\n");*/ return LE; }
"!="        { /*printf("NE\n");*/ return NE; }

"&&"        { /*printf("AND\n");*/ return AND; }
"||"        { /*printf("OR\n");*/ return OR; }
"!"         { /*printf("NOT\n");*/ return NOT; }
"&"         { /*printf("Single_and\n");*/ return SINGLE_AND; }
"%d"        { /*printf("Format_Int\n");*/ return FORMAT; }

{LETTER}([a-zA-Z_]|{DIGIT})*           { yylval.str = strdup(yytext); /*printf("ID: %s\n; ", yytext);*/ return IDENTIFIER; }

(([1-9]){DIGIT}*)|0                    { yylval.str = strdup(yytext); /*printf("CONST TYPE: int VALUE: %s\n", yylval.str);*/ return CONST; }

\"(\\.|\"\"|[^"\n"])*\"                { yylval.str = strdup(yytext); /*printf("String\n");*/ return STRING; }

[/][*][^*]*[*]+([^*/][^*]*[*]+)*[/]    {}
"//"[^\n]*                             {}
{WHITE_SPACE}                          {}
.                                      { /*printf("error");*/ }
%%