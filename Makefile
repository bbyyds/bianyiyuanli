compiler: scanner.l parser.y AST.cpp 
	g++ -c AST.cpp
	bison -d parser.y
	flex -oscanner.lex.c scanner.l
	sed -i "1i\#include \"AST.h\"" parser.tab.h
	g++ -o compiler AST.o parser.tab.c scanner.lex.c
	./compiler base.cpp
clean: 
	rm *.tab.* *.lex.c *.o