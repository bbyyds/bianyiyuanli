%{
#include "AST.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;
extern char *yytext;
extern int yylex();
extern void yyerror(char* s);
extern FILE * yyin;
AST* root;
%}

// 声明符号的所有可能拥有的类型
%union{
    AST* ast;
    char* str;
}

// 声明终结符
%token <str> IDENTIFIER
%token <str> SCANF
%token <str> PRINTF
%token <str> CONST
%token <str> STRING
%token <str> FORMAT
%token EOL

// 声明左结合终结符
%left '[' ']'
%left '(' ')'
%left <ast> ADD SUB MUL DIV MOD DOUBLE_ADD
%left <ast> AND OR SINGLE_AND
%left <str> EQ GT LT GE LE NE

// 声明右结合终结符
%right <ast> NOT
%right <str> ASSIGN

// 声明无结合终结符
%nonassoc '{' '}' COMMA SEMICOLON D_QUOTES 
%nonassoc INT FOR WHILE IF ELSE RETURN LOWER_THAN_ELSE

// 声明非终结符
%type <ast> Program         // 程序
%type <ast> BlockList       // 代码块列表
%type <ast> Block           // 代码块
%type <ast> Def             // 定义
%type <ast> DefList         // 定义列表
%type <ast> Stmt            // 语句
%type <ast> StmtList        // 语句列表
%type <ast> Var             // 变量
%type <ast> VarDef          // 变量定义
%type <ast> Exp             // 表达式
%type <ast> Specifier       // 说明符
%type <ast> Compst          // 函数体
%type <ast> ForDef          // for结构中变量定义
%type <ast> Func            // 函数
%type <ast> VarList         // 变量列表
%type <ast> Param           // 形参
%type <ast> Consts          // 常数
%type <ast> Args            // 实参

// $$符号引用产生式左侧非终结符的属性值
// $i符号引用产生式右侧第i个文法符号的属性值
%%
Program:
    BlockList{
        root = new AST(ROOT, "Program");
        root->appendFirst($1);
        printTree(root);
        printf("program\n");
    }
;
BlockList:
    Block{
        AST* node = new AST(STMT, "Block");
        node->appendFirst($1);
        $$ = node;
        printf("Block\n");
    }
    | BlockList Block{
        AST* node = new AST(STMT, "Blocks");
        node->appendFirst($1);
        $1->appendFollow($2);
        $$ = node;
        printf("BlockList\n");
    }
;
Block:
    Specifier DefList SEMICOLON{
        AST* node = new AST(DEF, "Block_Def");
        node->appendFirst($1);
        $1->appendFollow($2);
        $$ = node;
        printf("Block_Def\n");
    }
    | Specifier Func Compst{
        AST* node = new AST(DEF, "Block_FuncDef");
        node->appendFirst($1);
        $1->appendFollow($2);
        $2->appendFollow($3);
        $$ = node;
        printf("Block_FuncDec\n");
    }
    | Specifier Func SEMICOLON{
        AST* node = new AST(DEF, "Block_Func");
        node->appendFirst($1);
        $1->appendFollow($2);
        $$ = node;
        printf("Block_Func\n");
    }
;
VarDef:
    IDENTIFIER{
        AST* node = new AST(DEF, "VarDec_ID");
        AST* var_node = new AST(ID, $1);
        node->appendFirst(var_node);
        $$ = node;
        printf("VarDec_ID\n");
    }
    | IDENTIFIER '[' CONST ']'{
        AST* node = new AST(ARRAY,"array_id[const]");
        AST* const_node = new AST(CONST_INT, $3);
        AST* var_node = new AST(ID, $1);
        node->appendFirst(var_node);
        var_node->appendFollow(const_node);
        $$ = node;
        printf("vardef->identifier[const] \n");
    }
    | IDENTIFIER '[' Exp ']'{
        AST* node = new AST(ARRAY,"array_id[exp]");
        AST* var_node = new AST(ID, $1);
        node->appendFirst(var_node);
        var_node->appendFollow($3);
        $$ = node;
        printf("vardef->identifier[exp] \n");
    }
    | IDENTIFIER '[' ']'{
        AST* node = new AST(ARRAY,"array_id[]");
        AST* var_node = new AST(ID, $1);
        node->appendFirst(var_node);
        $$ = node;
        printf("vardef->identifier[] \n");
    }
    | MUL IDENTIFIER{
        AST* node = new AST(ARRAY,"array_*id");
        AST* var_node = new AST(ID, $2);
        node->appendFirst(var_node);
        $$ = node;
        printf("vardef->*identifier \n");
    }
;
Consts:
    Consts COMMA CONST{
        AST* node = new AST(EXPR,"consts_array");
        AST* const_node = new AST(CONST_INT, $3);
        node->appendFirst($1);
        $1->appendFollow(const_node);
        $$ = node;
        printf("consts->consts const \n");
    }
    | CONST{
        AST* node = new AST(EXPR,"const_array");
        AST* const_node = new AST(CONST_INT, $1);
        node->appendFirst(const_node);
        $$ = node;
        printf("consts->const \n");
    }
;
Specifier:
    INT{
        AST* node = new AST(MODIFY, "Specifier_INT");
        $$ = node;
        printf("Specifier_INT\n");
    }
;
Func:
    IDENTIFIER '(' ')'{
        AST* node = new AST(DEF, "Func_ID");
        AST* id_node = new AST(ID, $1);
        node->appendFirst(id_node);
        $$ = node;
        printf("Func_ID\n");
    }
    | IDENTIFIER '(' VarList ')'{
        AST* node = new AST(DEF,"Func_Params");
        AST* id_node = new AST(ID, $1);
        $$ = node;
        printf("Func_ID\n");
    }
;
VarList:
    VarList COMMA Param{
        AST* node = new AST(DEF,"Some_Param");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("varlist->varlist,param\n");
    }
  | Param{
        AST* node = new AST(DEF,"Single_Param");
        node->appendFirst($1);
        $$ = node;
        printf("varlist->param \n");
    }
;
Param:
    Specifier IDENTIFIER{
        AST* node = new AST(DEF,"Param_ID");
        AST* id_node = new AST(ID, $2);
        node->appendFirst($1);
        $1->appendFollow(id_node);
        $$ = node;
        printf("param->descriptor identifier \n");
    }
    | Specifier IDENTIFIER '[' ']'{
        AST* node = new AST(DEF,"Param_ID[]");
        AST* id_node = new AST(ID, $2);
        node->appendFirst($1);
        $1->appendFollow(id_node);
        $$ = node;
        printf("param->descriptor identifier[]\n");
    }
    | Specifier IDENTIFIER '[' CONST ']'{
        AST* node = new AST(DEF,"Param_ID[const]");
        AST* id_node = new AST(ID, $2);
        AST* const_node = new AST(CONST_INT, $4);
        node->appendFirst($1);
        $1->appendFollow(id_node);
        id_node->appendFollow(const_node);
        $$ = node;
        printf("param->descriptor identifier[const] \n");
    }
    | Specifier SINGLE_AND IDENTIFIER{
        AST* node = new AST(ARRAY,"array_&id");
        AST* var_node = new AST(ID, $3);
        node->appendFirst($1);
        $1->appendFollow(var_node);
        $$ = node;
        printf("param->&identifier \n");
    }
    | Specifier MUL IDENTIFIER{
        AST* node = new AST(ARRAY,"array_*id");
        AST* var_node = new AST(ID, $3);
        node->appendFirst($1);
        $1->appendFollow(var_node);
        $$ = node;
        printf("param->*identifier \n");
    }
    | Specifier {
        AST* node = new AST(DEF,"Param_NID");
        node->appendFirst($1);
        $$ = node;
        printf("param->descriptor \n");
    }
;
Compst:
    '{' StmtList '}'{
       AST* node = new AST(COMPST, "Compst");
       node->appendFirst($2);
       $$ = node;
       printf("Compst\n");
    }
;
StmtList:
    StmtList Stmt{
        AST* node = new AST(STMT, "StmtList");
        if ($1 == NULL){
            node->appendFirst($2);
        }
        else{
            node->appendFirst($1);
            $1->appendFollow($2);
        }
        $$ = node;
        printf("StmtList\n");
    }
    | {
        $$  = NULL;
        printf("No_Stmt\n");
    }
;
Stmt:
    Exp SEMICOLON{
        AST* node = new AST(STMT, "Stmt_Exp");
        node->appendFirst($1);
        $$ = node;
        printf("Stmt_Exp\n");
    }
    | Def SEMICOLON{
        AST* node = new AST(STMT, "Stmt_Def");
        node->appendFirst($1);
        $$ = node;
        printf("Stmt_DefList\n");
    }
    | Compst{
        AST* node = new AST(STMT, "Stmt_Compst");
        node->appendFirst($1);
        $$ = node;
        printf("Stmt_Compst\n");
    }
    | RETURN Exp SEMICOLON{
        AST* node = new AST(STMT, "Stmt_Return");
        node->appendFirst($2);
        $$ =  node;
        printf("Stmt_Return\n");
    }
    | RETURN SEMICOLON{
        AST* node = new AST(STMT, "Stmt_Return_Void");
        $$ = node;
        printf("Stmt_Return_Void\n");
    }
    | IF '(' Exp ')' Stmt{
        AST* node = new AST(STMT, "Stmt_If");
        node->appendFirst($3);
        $3->appendFollow($5);
        $$ = node;
        printf("Stmt_If\n");
    }
    | IF '(' Exp ')' Stmt ELSE Stmt %prec LOWER_THAN_ELSE{
        AST* node = new AST(STMT, "Stmt_If_Else");
        node->appendFirst($3);
        $3->appendFollow($5);
        $5->appendFollow($7);
        $$ = node;
        printf("Stmt_If_Else\n");
    }
    | WHILE '(' Exp ')' Stmt{
        AST* node = new AST(STMT, "Stmt_While");
        node->appendFirst($3);
        $3->appendFollow($5);
        $$ = node;
        printf("Stmt_While\n");
    }
    | FOR '(' SEMICOLON SEMICOLON ')' Stmt{
        AST* node = new AST(STMT, "Stmt_For_SEMI_SEMI");
        node->appendFirst($6);
        $$ = node;
        printf("Stmt_For_SEMI_SEMI\n");
    }
    | FOR '(' ForDef SEMICOLON SEMICOLON ')' Stmt{
        AST* node = new AST(STMT, "Stmt_For_Def_SEMI_SEMI");
        node->appendFirst($3);
        $3->appendFirst($7);
        $$ = node;
        printf("Stmt_For_Def_SEMI_SEMI\n");
    }
    | FOR '(' ForDef SEMICOLON Exp SEMICOLON Exp ')' Stmt{
        AST* node = new AST(STMT, "Stmt_For_Def_SEMI_Exp_SEMI_Exp");
        node->appendFirst($3);
        $3->appendFollow($5);
        $5->appendFollow($7);
        $7->appendFollow($9);
        $$ = node;
        printf("Stmt_For_Def_SEMI_Exp_SEMI_Exp\n");
    }
    | FOR '(' ForDef SEMICOLON SEMICOLON Exp ')' Stmt{
        AST* node = new AST(STMT, "Stmt_For_Def_SEMI_SEMI_Exp");
        node->appendFirst($3);
        $3->appendFollow($6);
        $6->appendFollow($8);
        $$ = node;
        printf("Stmt_For_Def_SEMI_SEMI_Exp\n");
    }
    | FOR '(' SEMICOLON Exp SEMICOLON Exp ')' Stmt{
        AST* node = new AST(STMT, "Stmt_For_SEMI_Exp_SEMI_Expn");
        node -> appendFirst($4);
        node -> appendFollow($6);
        $$ = node;
        printf("Stmt_For_SEMI_Exp_SEMI_Exp\n");
    }
    | SCANF '(' STRING COMMA Exp ')' SEMICOLON{
        cout << $3 << endl;
        AST* node = new AST(STMT, "Stmt_Scanf");
        AST* format_node = new AST(FORMAT_INT, $3);
        node->appendFirst(format_node);
        format_node->appendFollow($5);
        $$ = node;
        printf("Stmt_Scanf\n");
    }
    | PRINTF '(' STRING COMMA Exp ')' SEMICOLON{
        cout << $3 << endl;
        AST* node = new AST(STMT, "Stmt_Printf_Str_Exp");
        AST* str_node = new AST(CONST_STR, $3);
        node->appendFirst(str_node);
        str_node->appendFollow($5);
        $$ = node;
        printf("Stmt_Printf_Str_Exp\n");
    }
    | PRINTF '(' STRING ')' SEMICOLON{
        cout << $3 << endl;
        AST* node = new AST(STMT, "Stmt_Printf_Str");
        AST* str_node = new AST(CONST_STR, $3);
        node->appendFirst(str_node);
        $$ = node;
        printf("Stmt_Printf_Str\n");
    }
;
Def:
    Specifier DefList{
        AST* node = new AST(DEF, "Specifier_Def");
        node->appendFirst($1);
        $1->appendFollow($2);
        $$ = node;
        printf("Specifier_Def\n");
    }
;
DefList:
    Var{
        AST* node = new AST(DEF, "Def_Var");
        node->appendFirst($1);
        $$ = node;
        printf("Def_Var\n");
    }
    | Var COMMA DefList{
        AST* node = new AST(DEF, "Def_Vars");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("Def_Vars\n");
    }
;
Var:
    VarDef{
        AST* node = new AST(DEF, "VarDec");
        node->appendFirst($1);
        $$ = node;
        printf("VarDec\n");
    }
    | VarDef ASSIGN Exp{
        AST* node = new AST(DEF, "VarDec_Def");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("VarDec_Def\n");
    }
;
ForDef:
    Def {
        AST* node = new AST(DEF, "ForDef_Def");
        node->appendFirst($1);
        $$ = node;
        printf("ForDef_DefList\n");
    }
    | Exp{
        AST* node = new AST(DEF, "ForDef_Exp");
        node->appendFirst($1);
        $$ = node;
        printf("ForDef_Exp\n");
    }
;
Exp:
    CONST{
        AST* node = new AST(EXPR, "Exp_Const");
        AST* const_node = new AST(CONST_INT, $1);
        node->appendFirst(const_node);
        $$ = node;
        printf("Exp_Const\n");
    }
    | IDENTIFIER{
        AST* node = new AST(DEF, "Exp_ID");
        AST* id_node = new AST(ID, $1);
        node->appendFirst(id_node);
        $$ = node;
        printf("Exp_ID\n");
    }
    | Exp ASSIGN Exp{
        AST* node = new AST(OP, "Exp_Assign");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("Exp_Assign\n");
    }
    | Exp ADD Exp{
        AST* node = new AST(OP, "Exp_Add");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("Exp_Add\n");
    }
    | Exp SUB Exp{
        AST* node = new AST(OP, "Exp_Sub");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("Exp_Sub\n");
    }
    | Exp MUL Exp{
        AST* node = new AST(OP, "Exp_Mul");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("Exp_Mul\n");
    }
    | Exp DIV Exp{
        AST* node = new AST(OP, "Exp_Div");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("Exp_Div\n");
    }
    | Exp MOD Exp{
        AST* node = new AST(OP, "Exp_Mod");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("Exp_Mod\n");
    }
    | Exp DOUBLE_ADD {
        AST* node = new AST(OP, "Exp_DoubleAdd");
        node->appendFirst($1);
        $$ = node;
        printf("Exp_DoubleAdd\n");
    }
    | '(' Exp ')'{
        AST* node = new AST(OP, "Exp_(Exp)");
        node->appendFirst($2);
        $$ = node;
        printf("Exp_(Exp)\n");
    }
    | SUB Exp{
        AST* node = new AST(OP, "Exp_Negative");
        node->appendFirst($2);
        $$ = node;
        printf("Exp_Negative\n");
    }
    | Exp AND Exp{
        AST* node = new AST(OP, "Exp_And");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("Exp_And\n");
    }
    | Exp OR Exp{
        AST* node = new AST(OP, "Exp_Or");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("Exp_Or\n");
    }
    | NOT Exp{
        AST* node = new AST(OP, "Exp_Not");
        node->appendFirst($2);
        $$ = node;
        printf("Exp_Not\n");
    }
    | '{' Consts '}'{
        AST* node = new AST(ARRAY,"{consts}");
        node->appendFirst($2);
        $$ = node;
        printf("expr->{consts}\n");
    }
    | Exp EQ Exp{
        AST* node = new AST(OP, "EQ");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("EQ\n");
    }
    | Exp GT Exp{
        AST* node = new AST(OP, "GT");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("GT\n");
    }
    | Exp LT Exp{
        AST* node = new AST(OP, "LT");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("LT\n");
    }
    | Exp GE Exp{
        AST* node = new AST(OP, "GE");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("GE\n");
    }
    | Exp LE Exp{
        AST* node = new AST(OP, "LE");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("LE\n");
    }
    | Exp NE Exp{
        AST* node = new AST(OP, "NE");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("NE\n");
    }
    | IDENTIFIER '(' Args ')' {
        AST* node = new AST(CALL, "Call_Args_Func");
        AST* id_node = new AST(ID, $1);
        node->appendFirst(id_node);
        id_node->appendFollow($3);
        $$ = node;
        printf("exp->id '(' Args ')' \n");
    }
    | IDENTIFIER '(' ')' {
        AST* node = new AST(CALL, "Call_NArgs_Func");
        AST* id_node = new AST(ID, $1);
        node->appendFirst(id_node);
        $$ = node;
        printf("exp->id '('  ')' \n");
    }
    | MUL IDENTIFIER {
        AST* node = new AST(OP,"*id");
        AST* id_node = new AST(ID, $2);
        node->appendFirst(id_node);
        $$ = node;
        printf("Exp_*id\n")
    }
    | IDENTIFIER '[' Exp ']' {
        AST* node = new AST(OP,"id[exp]");
        AST* id_node = new AST(ID, $1);
        node->appendFirst(id_node);
        id_node->appendFollow($3);
        $$ = node;
        printf("Exp_id[Exp]\n");
    }
    | SINGLE_AND IDENTIFIER {
        AST* node = new AST(OP, "Single_And");
        AST* id_node = new AST(ID, $2);
        node->appendFirst(id_node);
        $$ = node;
        printf("Single_And\n");
    }
;
Args: 
    Exp {
        AST* node = new AST(ID,"Func_Single_Arg");
        node->appendFirst($1);
        $$ = node;
        printf("Args_Exp\n");
    }
    | Args COMMA Exp {
        AST* node = new AST(DEF,"Func_Some_Args");
        node->appendFirst($1);
        $1->appendFollow($3);
        $$ = node;
        printf("Args_args_exp\n")
    }
;

%%
int main(int argc, char* argv[]){
    if(argc > 1){
        if(!(yyin = fopen(argv[1], "r"))){
            perror(argv[1]);
            return 1;
        }
    }
    while(!feof(yyin)){
        yyparse();
    }
    return 0;
}

void yyerror(char* s){
    fprintf(stderr, "error: %s\n", s);
}
